import React from 'react';
import { AppRegistry, StatusBar, View } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';

import { store, persistor } from './src/redux/store';

import { name as appName } from './app.json';
import styles, { COLOR } from "./src/styles";
import { AlertView, SnackBar, ReduxLoader } from "./src/components";

import Index from './src/scenes';

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <View style={styles.f1}>
          <StatusBar
            backgroundColor={COLOR.APP_DARK}
            barStyle="light-content"
          />
          <Index/>
          <AlertView id={'Alert_Root_App'}/>
          <SnackBar id={'SnackBar_Root_App'}/>
          <ReduxLoader/>
        </View>
      </PersistGate>
    </Provider>
  );
};

AppRegistry.registerComponent(appName, () => App);
