import React from 'react';
import { Text } from 'react-native';
import styles from "../styles";

export default (props) => {
  const { children, style = {} } = props;
  return (
    <Text {...props} style={[styles.fontRoboto, styles.cWhite, style]}>
      {children ? children : ''}
    </Text>
  );
};

