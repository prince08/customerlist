import React from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import Loading from "./loading";
import styles from "../styles";

class Loader extends React.PureComponent {
  render() {
    const { showLoader } = this.props;
    const style = [styles.absolute, styles.bgTransparent, styles.center, styles.l0, styles.b0, styles.t0, styles.r0];

    if (!showLoader) {
      return <View/>;
    }
    return (
      <View style={style}>
        <Loading/>
      </View>
    );
  }
}

export default connect(({ showLoader }) => ( { showLoader } ))(Loader);
