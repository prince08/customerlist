import { connect } from 'react-redux';
import CustomerList from './customerList';
import { getCustomerList, deleteCustomer, getSortedData, getSearchedData } from '../../redux/actions';

const mapStateToProps = ({ customerList }) => ( { customerList } );
const mapDispatchToProps = { getCustomerList, deleteCustomer, getSortedData, getSearchedData };

export default connect(mapStateToProps, mapDispatchToProps)(CustomerList);