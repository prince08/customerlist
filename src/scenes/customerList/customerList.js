import React from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';

import { Icon, Input, UIText } from '../../components';
import { ADD_CUSTOMER } from "../../utilities";
import styles, { COLOR, width } from '../../styles';

const IconButton = ({ onPress = () => ( {} ), name = 'add', color = COLOR.WHITE, size = 24 }) => {
  return (
    <TouchableOpacity style={[styles.ph10, styles.center]} onPress={onPress}>
      <Icon name={name} color={color} size={size}/>
    </TouchableOpacity>
  );
};

const Button = ({ onPress, text = 'add', color = COLOR.WHITE }) => {
  return (
    <TouchableOpacity style={[styles.ph10, styles.center]} onPress={onPress}>
      <UIText style={{ color }}>{text}</UIText>
    </TouchableOpacity>
  );
};

export default class CustomerList extends React.Component {
  static navigationOptions = ({ navigation }) => ( {
    title: 'Customer list',
    headerRight: <IconButton name={'add'} onPress={() => navigation.navigate(ADD_CUSTOMER)}/>
  } );

  state = { searchText: '' };
  searchTimeout = null;

  componentDidMount() {
    const { getCustomerList } = this.props;
    getCustomerList();
  }

  sortData = (isAscendingOrder = true) => {
    const { getSortedData } = this.props;
    getSortedData(isAscendingOrder);
  };

  search = (text) => {
    this.searchTimeout && clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      const { getSearchedData } = this.props;
      getSearchedData(text);
    }, 500);
  };

  deleteCustomer = (item, index) => {
    const { deleteCustomer } = this.props;
    deleteCustomer(item, index);
  };

  keyExtractor = ({ number }) => number.toString();

  itemSeparatorComponent = () => <View style={[styles.bgWhite, styles.h1]}/>;

  renderNoData = () => {
    const message = this.state.searchText ? 'No Matching records found' : 'No records found.';
    return (
      <UIText style={[styles.textCenter, styles.mt30]}>{message}</UIText>
    )
  };

  renderItem = ({ item, index }) => {
    const { firstName, lastName, emailId, phone } = item;
    const name = firstName || lastName || emailId.slice(0, emailId.indexOf('@'));

    return (
      <View style={[styles.flexRow, styles.ph5, styles.pv10]}>
        <View style={styles.f1}>
          <UIText style={[styles.font16]}>{name}</UIText>
          {emailId && <UIText style={[styles.font14, styles.ml10]}>{emailId}</UIText>}
          {phone && <UIText style={[styles.font14, styles.ml10]}>{phone}</UIText>}
        </View>
        <IconButton name={'close'} onPress={this.deleteCustomer.bind(this, item, index)}/>
      </View>
    )
  };

  render() {
    const { customerList = [] } = this.props;

    return (
      <View style={[styles.f1, styles.p5, styles.bgApp]}>
        <View style={[styles.flexRow, styles.jCenter]}>
          <Input
            type="text"
            width={width - 140}
            placeholder={'search'}
            borderBottomColor={COLOR.WHITE}
            inputStyle={styles.cWhite}
            placeholderTextColor={'rgba(255,255,255,0.5)'}
            onChangeText={this.search}
          />
          {/*<IconButton name={'search'} onPress={this.search}/>*/}
          <Button text={'A-Z'} onPress={this.sortData.bind(this, true)}/>
          <Button text={'Z-A'} onPress={this.sortData.bind(this, false)}/>
        </View>
        <FlatList
          data={customerList}
          keyExtractor={this.keyExtractor}
          ListEmptyComponent={this.renderNoData}
          ItemSeparatorComponent={this.itemSeparatorComponent}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}
