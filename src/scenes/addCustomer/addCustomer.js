import React from 'react';
import { Keyboard, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';

import { Alert, Icon, Input, UIText } from '../../components';
import styles, { COLOR } from '../../styles';

const defaultInputProps = {
  inputStyle: styles.cWhite,
  borderBottomColor: '#FFF',
  placeholderTextColor: 'rgba(255,255,255,0.5)',
};

export default class AddCustomer extends React.Component {
  static navigationOptions = {
    title: 'Add new customer'
  };

  state = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: ''
  };

  onSubmit = async () => {
    const { navigation, addNewCustomer } = this.props;
    const { firstName, lastName, email, phoneNumber } = this.state;
    if (!firstName || !lastName || !email || !phoneNumber) {
      return Alert({ title: 'Validation failed', message: 'Please enter all the required information of a customer.' });
    }
    return addNewCustomer(this.state, navigation);
  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={[styles.f1, styles.bgApp, styles.p20]}>

          <UIText style={[styles.cWhite]}>First name</UIText>
          <Input
            {...defaultInputProps}
            type="text"
            placeholder={'Jhon'}
            onChangeText={text => {
              this.state.firstName = text;
            }}
          />
          <UIText style={[styles.cWhite]}>First name</UIText>
          <Input
            {...defaultInputProps}
            type="text"
            placeholder={'Smith'}
            onChangeText={text => {
              this.state.lastName = text;
            }}
          />
          <UIText style={[styles.cWhite]}>Email</UIText>
          <Input
            {...defaultInputProps}
            type="email"
            placeholder={'example@example.com'}
            onChangeText={text => {
              this.state.email = text;
            }}
          />
          <UIText style={[styles.cWhite]}>Phone number</UIText>
          <Input
            {...defaultInputProps}
            type="number"
            placeholder={'8917625256'}
            onChangeText={text => {
              this.state.phoneNumber = text;
            }}
          />

          <TouchableOpacity
            activeOpacity={0.8}
            style={[styles.circle50, styles.mt20, styles.bgWhite, styles.shadow2, styles.center, { alignSelf: 'flex-end' }]}
            onPress={this.onSubmit}
          >
            <Icon name={'keyboard-arrow-right'} size={30} color={COLOR.APP}/>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
