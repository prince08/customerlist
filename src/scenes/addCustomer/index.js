import { connect } from 'react-redux';
import AddCustomer from './addCustomer';
import { addNewCustomer } from '../../redux/actions';

const mapStateToProps = () => ( {} );
const mapDispatchToProps = { addNewCustomer };

export default connect(mapStateToProps, mapDispatchToProps)(AddCustomer);