import React from 'react';
import { createStackNavigator } from 'react-navigation';

import AddCustomer from './addCustomer';
import CustomerList from './customerList';

import styles, { COLOR } from '../styles';
import { CUSTOMER_LIST, ADD_CUSTOMER } from '../utilities';

const ROUTE_CONFIG = {
  [CUSTOMER_LIST]: CustomerList,
  [ADD_CUSTOMER]: AddCustomer,
};
const STACK_NAVIGATOR_CONFIG = {
  initialRouteName: CUSTOMER_LIST,
  navigationOptions: {
    headerStyle: styles.bgAppDark,
    headerBackTitleStyle: COLOR.WHITE,
    headerTintColor: COLOR.WHITE,
    headerTitleStyle: [styles.cWhite, styles.fontRoboto]
  }
};

export default createStackNavigator(ROUTE_CONFIG, STACK_NAVIGATOR_CONFIG);
