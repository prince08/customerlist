import axios from 'axios';
import config from '../../config';

const { SERVER_NATIVE_URL, BUSINESS_ID, API_KEY } = config;
export const get = handleApiCallRN.bind(null, 'GET');
export const post = handleApiCallRN.bind(null, 'POST');
export const put = handleApiCallRN.bind(null, 'PUT');
export const del = handleApiCallRN.bind(null, 'DELETE');

function handleApiCallRN(method, url, data = {}, otherOptions = {}) {
  const options = {
    method,
    timeout: 5000,
    headers: {
      'accept': 'application/json',
      'content-type': 'application/json'
    },
    ...otherOptions
  };

  if (method !== 'GET') {
    options.data = data;
  }

  if (url.toLowerCase().indexOf('http') < 0) {
    url = SERVER_NATIVE_URL + url + '?businessId=' + BUSINESS_ID + '&api_key=' + API_KEY + '&bid=' + BUSINESS_ID;
  }

  options.url = url;
  // console.log("options :- ", options);
  return axios(options);
}
