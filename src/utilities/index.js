import { Dimensions } from 'react-native';
import * as Api from './api';
import * as Storage from './storage';

const { width, height } = Dimensions.get('window');

export { width, height, Api, Storage };
export * from './validations';
export * from './constants';
export * from './navigationService';

export const sortData = (data = [], isAscendingOrder = true) => {
  const sortingFunction = (a, b) => {
    const user1Name = a.firstName || a.lastName || a.emailId.slice(0, a.emailId.indexOf('@'));
    const user2Name = b.firstName || b.lastName || b.emailId.slice(0, b.emailId.indexOf('@'));
    return user1Name.toLocaleLowerCase() < user2Name.toLocaleLowerCase() ?
      isAscendingOrder ? -1 : 1
      : isAscendingOrder ? 1 : -1
  };

  return data.sort(sortingFunction);
};

export const searchData = (data, text) => {
  return data.filter(({ firstName, lastName, emailId }) => {
    if (firstName) {
      return firstName.toLocaleLowerCase().indexOf(text.toLocaleLowerCase()) > -1;
    }
    if (lastName) {
      return lastName.toLocaleLowerCase().indexOf(text.toLocaleLowerCase()) > -1;
    }
    return emailId.slice(0, emailId.indexOf('@')).toLocaleLowerCase().indexOf(text.toLocaleLowerCase()) > -1;
  })
};