import { CUSTOMER_LIST, OLD_CUSTOMER_LIST, SEARCH_TEXT, SHOW_LOADER } from "./reduxConstants";
import { Api, searchData, sortData } from '../utilities';
import { Alert } from "../components";

export function getCustomerList() {
  return async dispatch => {
    try {
      dispatch({ type: SHOW_LOADER, data: true });
      let { data } = await Api.get('/all');
      dispatch({ type: SHOW_LOADER, data: false });
      data = sortData(data, true);

      dispatch({ type: OLD_CUSTOMER_LIST, data });
      return dispatch({ type: CUSTOMER_LIST, data });
    } catch (err) {
      console.warn("error in getting customer list:", err);
      dispatch({ type: SHOW_LOADER, data: false });
    }
  };
}

export const getSortedData = (isAscending = true) => {
  return (dispatch, getState) => {
    let { customerList, oldCustomerList } = getState();
    customerList = sortData(customerList, isAscending);
    oldCustomerList = sortData(oldCustomerList, isAscending);
    dispatch({ type: OLD_CUSTOMER_LIST, data: [...oldCustomerList] });
    return dispatch({ type: CUSTOMER_LIST, data: [...customerList] });
  }
};

export const getSearchedData = (text) => {
  return (dispatch, getState) => {
    let { oldCustomerList } = getState();
    dispatch({ type: CUSTOMER_LIST, data: [...searchData(oldCustomerList, text)] });
    dispatch({ type: SEARCH_TEXT, data: text });
  }
};

export const deleteCustomer = ({ number }, index) => {
  return async (dispatch, getState) => {
    Alert({
      title: 'Delete Customer',
      message: 'Do you want to delete this customer',
      buttons: [{
        title: 'CANCEL'
      }, {
        title: 'OK', onPress: async () => {
          try {
            dispatch({ type: SHOW_LOADER, data: true });
            await Api.del('/id/' + number);
            dispatch({ type: SHOW_LOADER, data: false });
            const { customerList, oldCustomerList } = getState();

            customerList.splice(index, 1);
            oldCustomerList.splice(oldCustomerList.findIndex(item => item.number === number), 1);
            return dispatch({ type: CUSTOMER_LIST, data: [...customerList] });
          } catch (err) {
            console.log(" error here is ", err);
            dispatch({ type: SHOW_LOADER, data: false });
          }
        }
      }]
    });
  }
};

export const addNewCustomer = (data, navigation) => {
  return async (dispatch, getState) => {
    try {
      const { firstName, lastName, email, phoneNumber } = data;
      const payload = {
        name: `${firstName} ${lastName}`,
        emailId: email,
        // phone: phoneNumber 
      };

      dispatch({ type: SHOW_LOADER, data: true });
      const { data: { customerId = '' } } = await Api.post('/checkin', payload);
      let { oldCustomerList, searchText } = getState();

      oldCustomerList.push({ number: customerId, firstName, lastName, emailId: email, phone: phoneNumber });
      oldCustomerList = sortData(oldCustomerList, true);
      const newCustomerList = searchData(oldCustomerList, searchText);

      dispatch({ type: SHOW_LOADER, data: false });
      dispatch({ type: OLD_CUSTOMER_LIST, data: [...oldCustomerList] });
      dispatch({ type: CUSTOMER_LIST, data: [...newCustomerList] });
      return navigation.goBack();
    } catch (error) {
      console.log("error here is :- ", error);
      dispatch({ type: SHOW_LOADER, data: false });
    }
  }
};