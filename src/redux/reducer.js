import { CUSTOMER_LIST, OLD_CUSTOMER_LIST, SEARCH_TEXT, SHOW_LOADER } from "./reduxConstants";

export default function (state = {}, action) {
  const { type, data } = action;

  switch (type) {
    case CUSTOMER_LIST:
      return { ...state, customerList: data };
    case OLD_CUSTOMER_LIST:
      return { ...state, oldCustomerList: data };
    case SEARCH_TEXT:
      return { ...state, searchText: data };
    case SHOW_LOADER:
      return { ...state, showLoader: data };
    default:
      return state;
  }
}
