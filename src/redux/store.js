import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { persistReducer, persistStore } from 'redux-persist'

import storage from 'redux-persist/lib/storage'

import reducer from "./reducer";

const persistConfig = {
  key: 'root',
  storage,
};
const initial_state = {
  customerList: [],
  oldCustomerList: [],
  searchText: '',
  showLoader: false,
};
const persistedReducer = persistReducer(persistConfig, reducer);

const enhancer = compose(applyMiddleware(thunk));

export const store = createStore(persistedReducer, initial_state, enhancer);
export const persistor = persistStore(store);
